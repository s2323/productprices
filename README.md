# README #

Требуется написать gRPC-сервер на языке GoLang (1.13+), с постоянным хранилищем
MongoDB, реализующий 2 метода:

* Fetch(`URL`) - запросить внешний CSV-файл со списком продуктов по внешнему адресу.
CSV-файл имеет вид `PRODUCT NAME;PRICE`. Последняя цена каждого продукта должна
быть сохранена в базе с датой запроса. Также нужно сохранять количество изменений
цены продукта.
* List(`<paging params>`, `<sorting params>`) - получить постраничный список продуктов с их
ценами, количеством изменений цены и датами их последнего обновления.
Предусмотреть все варианты сортировки для реализации интерфейса в виде
бесконечного скролла.

Сервер должен быть запущен в 2+ экземплярах (каждый в своем Docker-контейнере) и
закрыт балансировщиком, соответствующие конфигурации также должны быть
предоставлены для тестовой среды.

### Сборка и запуск ###

```
#!bash
make build-docker
make dev-service-start
```

Выполнение этих команд приведёт к следующему:

* из исходных кодов будет скомпилирован сервис и помещён в Docker-образ
* docker-compose запустит MongoDB
* docker-compose запустит [MongoExpress](http://localhost:8081)
* будут применены (up) [миграции](https://bitbucket.org/s2323/productprices/src/master/internal/pkg/database/mongo/migrations/)
* docker-compose запустит два контейнера с сервисом (`--scale`)
* docker-compose запустит nginx в качестве балансировщика
* docker-compose запустит [Jaeger](http://localhost:16686) для трейсинга (несколько трейсов, как пример такой возможности)
* docker-compose запустит Prometheus для сбора метрик (несколько метрик, как пример такой возможности)
* docker-compose запустит [Grafana](http://localhost:3000/d/fl-qjgtMk/productprices-dashboard?orgId=1&refresh=5s) для просмотра метрик (логин/пароль - admin/admin)

Доступ к сервисам возможен только через балансировщик по следующим портам:

* `localhost:7000` - gRPC-сервер
* `localhost:7001` - http-сервер
* `localhost:7002` - отладочный сервер
* `localhost:8080` - http-сервер с тестовыми CSV-файлами (см. `scripts/dev/nginx/www`)

С помощью http-сервера и отладочного сервера можно потестировать сервис через браузер. Для этого необходимо перейти по ссылке http://localhost:7002/swagger/
и выполнить запрос:

* http://localhost:7002/swagger/#/ProductPrices/ProductPrices_Fetch - для обработки CSV-файла.
Данный запрос только отправляет задание на обработку, сама обработка осуществляется в асинхронноч режиме.
Это позволяет обрабатывать большие файлы не боясь потерять соединение с сервисом. Пример запроса:
```
{"URL": "http://nginx:8080/test_file_1.csv"}
```
* http://localhost:7002/swagger/#/ProductPrices/ProductPrices_List - для получения постраничного списка продуктов из обработанных файлов.
Если в качестве запроса использовать `{}`, будут выведены первые 20 продуктов (сортировка по названию).
Далее, для получения следующей страницы результатов, можно использовать объект `next` из ответа сервиса.

Пример запросов с использованием grpc_cli:

```
grpc_cli call --json_input --json_output localhost:7000 productprices.ProductPrices/Fetch '{"URL": "http://nginx:8080/test_file_1.csv"}'
```

```
grpc_cli call --json_input --json_output localhost:7000 productprices.ProductPrices/List '{}'
```


##### Остановка сервисов #####

```
make dev-service-stop
make dev-db-stop
```

##### Удаление контейнеров #####

```
make dev-service-rm`
make dev-db-rm
```

### Сборка и запуск без docker-compose ###

Для данного раздела потребуется база MongoDB с накатанными миграциями.

```
make build
export PRODUCTPRICES_MONGO_DATABASE = # название базы данных с применёнными миграциями
export PRODUCTPRICES_MONGO_USERNAME = # имя пользователя для доступа к базе данных
export PRODUCTPRICES_MONGO_PASSWORD = # пароль пользователя для доступа к базу данных
export PRODUCTPRICES_MONGO_HOST = # список хостов с серверами MongoDB (разделение запятыми)
export PRODUCTPRICES_MONGO_PORT = # порт сервера MongoDB (предполагается, что используется одинаковый порт на всех хостах)
./bin/productprices
```

Вместо `PRODUCTPRICES_MONGO_PASSWORD` можно сэмулировать Docker secrets и поместить пароль в файл `/run/secrets/mongo_password`.

### Запуск unit тестов ###

```
make test
```

### Запуск тестов сервиса ###

```
make test-service
```

### Запуск линтера ###

```
make lint
```
