module bitbucket.org/s2323/productprices/pkg/pb/productprices

go 1.14

require (
	github.com/gogo/protobuf v1.3.1
	github.com/golang/protobuf v1.4.2
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.0.0
	github.com/rakyll/statik v0.1.7
	google.golang.org/genproto v0.0.0-20201013134114-7f9ee70cb474
	google.golang.org/grpc v1.33.0
	google.golang.org/protobuf v1.25.0
)
