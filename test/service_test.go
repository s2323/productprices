package test

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	pb "bitbucket.org/s2323/productprices/pkg/pb/productprices"
)

func TestService(t *testing.T) {
	t.Run("fetch bad url", func(t *testing.T) {
		c := globalContext.getClient()

		_, err := c.Fetch(globalContext.ctx, &pb.FetchRequest{
			URL: "\n",
		})
		require.Error(t, err)
		require.Equal(t, codes.InvalidArgument, status.Code(err))
	})

	t.Run("fetch unknown file", func(t *testing.T) {
		c := globalContext.getClient()

		_, err := c.Fetch(globalContext.ctx, &pb.FetchRequest{
			URL: globalContext.s.URL + "/asdasdasd",
		})
		require.NoError(t, err)
	})

	time.Sleep(time.Second)

	t.Run("list after fetch bad url and unknown file", func(t *testing.T) {
		c := globalContext.getClient()

		resp, err := c.List(globalContext.ctx, &pb.ListRequest{
			Paging: &pb.ListRequest_PagingParams{
				Page:     1,
				PageSize: 1,
			},
		})
		require.NoError(t, err)
		require.NotNil(t, resp)
		require.Zero(t, resp.Total)
	})

	t.Run("fetch test_file_1.csv", func(t *testing.T) {
		c := globalContext.getClient()

		_, err := c.Fetch(globalContext.ctx, &pb.FetchRequest{
			URL: globalContext.s.URL + "/test_file_1.csv",
		})
		require.NoError(t, err)
	})

	time.Sleep(time.Second)

	t.Run("list after fetch test_file_1.csv", func(t *testing.T) {
		c := globalContext.getClient()

		resp, err := c.List(globalContext.ctx, &pb.ListRequest{
			Paging: &pb.ListRequest_PagingParams{
				Page:     1,
				PageSize: 10,
			},
		})
		require.NoError(t, err)
		require.NotNil(t, resp)
		require.Nil(t, resp.Next)
		require.Equal(t, uint64(5), resp.Total)
		require.Len(t, resp.Products, 5)
		for i, product := range resp.Products {
			require.Equal(t, fmt.Sprintf("product #%d", i+1), product.Name)
			require.Equal(t, float64(i+1)*100, product.Price)
			require.NotZero(t, product.UpdatedAt)
			require.Zero(t, product.UpdatesCount)
		}
	})

	t.Run("fetch test_file_2.csv", func(t *testing.T) {
		c := globalContext.getClient()

		_, err := c.Fetch(globalContext.ctx, &pb.FetchRequest{
			URL: globalContext.s.URL + "/test_file_2.csv",
		})
		require.NoError(t, err)
	})

	time.Sleep(time.Second)

	t.Run("list after fetch test_file_2.csv", func(t *testing.T) {
		c := globalContext.getClient()

		resp, err := c.List(globalContext.ctx, &pb.ListRequest{
			Paging: &pb.ListRequest_PagingParams{
				Page:     1,
				PageSize: 10,
			},
			Sorting: []*pb.ListRequest_SortingParams{
				{
					OrderBy: pb.SortOrder_SORT_ORDER_BY_UPDATED_AT,
					Desc:    true,
				},
				{
					OrderBy: pb.SortOrder_SORT_ORDER_BY_NAME,
					Desc:    true,
				},
			},
		})
		require.NoError(t, err)
		require.NotNil(t, resp)
		require.Nil(t, resp.Next)
		require.Equal(t, uint64(5), resp.Total)
		require.Len(t, resp.Products, 5)

		require.Equal(t, "product #3", resp.Products[0].Name)
		require.Equal(t, "product #2", resp.Products[1].Name)
		require.Equal(t, "product #1", resp.Products[2].Name)
		require.Equal(t, "product #5", resp.Products[3].Name)
		require.Equal(t, "product #4", resp.Products[4].Name)
		for _, product := range resp.Products {
			require.Zero(t, product.UpdatesCount)
		}
	})

	t.Run("fetch test_file_3.csv", func(t *testing.T) {
		c := globalContext.getClient()

		_, err := c.Fetch(globalContext.ctx, &pb.FetchRequest{
			URL: globalContext.s.URL + "/test_file_3.csv",
		})
		require.NoError(t, err)
	})

	time.Sleep(time.Second)

	t.Run("list after fetch test_file_3.csv", func(t *testing.T) {
		c := globalContext.getClient()

		resp, err := c.List(globalContext.ctx, &pb.ListRequest{
			Paging: &pb.ListRequest_PagingParams{
				Page:     1,
				PageSize: 10,
			},
			Sorting: []*pb.ListRequest_SortingParams{
				{
					OrderBy: pb.SortOrder_SORT_ORDER_BY_UPDATED_AT,
					Desc:    true,
				},
				{
					OrderBy: pb.SortOrder_SORT_ORDER_BY_NAME,
				},
			},
		})
		require.NoError(t, err)
		require.NotNil(t, resp)
		require.Nil(t, resp.Next)
		require.Equal(t, uint64(5), resp.Total)
		require.Len(t, resp.Products, 5)

		require.Equal(t, "product #4", resp.Products[0].Name)
		require.Equal(t, "product #5", resp.Products[1].Name)
		require.Equal(t, "product #1", resp.Products[2].Name)
		require.Equal(t, "product #2", resp.Products[3].Name)
		require.Equal(t, "product #3", resp.Products[4].Name)

		require.Equal(t, uint64(1), resp.Products[0].UpdatesCount)
		require.Equal(t, uint64(1), resp.Products[0].UpdatesCount)
	})

	t.Run("fetch test_file_with_header.csv", func(t *testing.T) {
		c := globalContext.getClient()

		_, err := c.Fetch(globalContext.ctx, &pb.FetchRequest{
			URL: globalContext.s.URL + "/test_file_with_header.csv",
		})
		require.NoError(t, err)
	})

	time.Sleep(time.Second)

	t.Run("list after fetch test_file_with_header.csv", func(t *testing.T) {
		c := globalContext.getClient()

		resp, err := c.List(globalContext.ctx, &pb.ListRequest{
			Paging: &pb.ListRequest_PagingParams{
				Page:     1,
				PageSize: 3,
			},
			Sorting: []*pb.ListRequest_SortingParams{
				{
					OrderBy: pb.SortOrder_SORT_ORDER_BY_PRICE,
					Desc:    true,
				},
			},
		})
		require.NoError(t, err)
		require.NotNil(t, resp)
		require.NotNil(t, resp.Next)
		require.Equal(t, uint64(6), resp.Total)
		require.Len(t, resp.Products, 3)

		require.Equal(t, "xxx", resp.Products[0].Name)
		require.Equal(t, "product #4", resp.Products[1].Name)
		require.Equal(t, "product #5", resp.Products[2].Name)

		require.Equal(t, 100500.0, resp.Products[0].Price)
		require.Equal(t, 500.0, resp.Products[1].Price)
		require.Equal(t, 400.0, resp.Products[2].Price)

		resp, err = c.List(globalContext.ctx, resp.Next)
		require.NoError(t, err)
		require.NotNil(t, resp)
		require.Nil(t, resp.Next)
		require.Equal(t, uint64(6), resp.Total)
		require.Len(t, resp.Products, 3)

		require.Equal(t, "product #3", resp.Products[0].Name)
		require.Equal(t, "product #2", resp.Products[1].Name)
		require.Equal(t, "product #1", resp.Products[2].Name)

		require.Equal(t, 300.0, resp.Products[0].Price)
		require.Equal(t, 200.0, resp.Products[1].Price)
		require.Equal(t, 100.0, resp.Products[2].Price)
	})
}
