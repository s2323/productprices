package test

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	"github.com/posener/ctxutil"
	"google.golang.org/grpc"

	"bitbucket.org/s2323/productprices/cmd/productprices/app"
	pb "bitbucket.org/s2323/productprices/pkg/pb/productprices"
)

type RuntimeContext struct {
	ctx    context.Context
	conn   *grpc.ClientConn
	client pb.ProductPricesClient
	s      *httptest.Server
}

var globalContext = RuntimeContext{
	ctx: ctxutil.Interrupt(),
}

func TestMain(m *testing.M) {
	var err error
	globalContext.conn, err = grpc.Dial("127.0.0.1:7000", grpc.WithInsecure())
	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "error: %s\n", err.Error())
		_ = os.Stderr.Sync()
		os.Exit(1)
	}
	defer func() {
		_ = globalContext.conn.Close()
	}()

	globalContext.s = httptest.NewServer(http.FileServer(http.Dir("./testdata")))
	defer func() {
		globalContext.s.Close()
	}()

	go app.Run()
	waitForService()

	code := m.Run()
	os.Exit(code)
}

func waitForService() {
	ctx, cancel := context.WithTimeout(globalContext.ctx, 10*time.Second)
	defer cancel()

	for {
		r, err := (&http.Client{}).Get("http://localhost:7002/version")
		if err == nil && r.StatusCode == http.StatusOK {
			break
		}
		if ctx.Err() != nil {
			break
		}
		time.Sleep(time.Second)
	}
}

func (r RuntimeContext) getClient() pb.ProductPricesClient {
	if r.client == nil {
		r.client = pb.NewProductPricesClient(r.conn)
	}
	return r.client
}
