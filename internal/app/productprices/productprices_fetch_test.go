package productprices

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"

	pb "bitbucket.org/s2323/productprices/pkg/pb/productprices"
)

func TestImplementation_Fetch(t *testing.T) {
	url := "http://test/test.csv"

	t.Run("fetch ok", func(t *testing.T) {
		fx := tearUp(t)

		fx.queue.On("Push", mock.Anything, url).Return(nil)

		_, err := fx.service.Fetch(fx.ctx, &pb.FetchRequest{URL: url})
		require.NoError(t, err)
	})

	t.Run("fetch with bad url", func(t *testing.T) {
		fx := tearUp(t)

		_, err := fx.service.Fetch(fx.ctx, &pb.FetchRequest{URL: "\n"})
		require.Error(t, err)
	})

	t.Run("fetch with queue failure", func(t *testing.T) {
		fx := tearUp(t)

		expErr := assert.AnError
		fx.queue.On("Push", mock.Anything, url).Return(expErr)

		_, err := fx.service.Fetch(fx.ctx, &pb.FetchRequest{URL: url})
		require.Error(t, err)
		require.Equal(t, expErr, err)
	})
}
