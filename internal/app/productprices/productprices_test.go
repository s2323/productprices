package productprices

import (
	"context"
	"testing"

	"github.com/posener/ctxutil"

	"bitbucket.org/s2323/productprices/internal/app/productprices/mocks"
)

type fixture struct {
	t       *testing.T
	ctx     context.Context
	storage *mocks.Storage
	queue   *mocks.Queue
	service *Implementation
}

func tearUp(t *testing.T) *fixture {
	fx := &fixture{
		t:       t,
		ctx:     ctxutil.Interrupt(),
		storage: &mocks.Storage{},
		queue:   &mocks.Queue{},
	}
	fx.service = New(fx.queue, fx.storage)

	fx.storage.Test(t)
	fx.queue.Test(t)
	t.Cleanup(func() {
		fx.storage.AssertExpectations(t)
		fx.queue.AssertExpectations(t)
	})
	return fx
}
