package productprices

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"

	"bitbucket.org/s2323/productprices/internal/pkg/model"
	pb "bitbucket.org/s2323/productprices/pkg/pb/productprices"
)

func TestImplementation_List(t *testing.T) {
	t.Run("list ok", func(t *testing.T) {
		fx := tearUp(t)

		page := uint32(1)
		pageSize := uint32(20)
		sorting := model.SortOrderByPrice
		product := model.Product{
			Name:         "xxx",
			Price:        100,
			UpdatesCount: 1,
			UpdatedAt:    123,
		}

		fx.storage.On("GetProductsPage", mock.Anything, model.ProductsListRequest{
			Paging: model.Paging{
				Page:     page,
				PageSize: pageSize,
			},
			Sorting: []model.Sorting{
				{
					OrderBy: sorting,
					Desc:    true,
				},
			},
		}).Return(model.ProductsListResponse{
			Products: []model.Product{product},
			Total:    100,
			Next: &model.ProductsListRequest{
				Paging: model.Paging{
					Page:     page + 1,
					PageSize: pageSize,
				},
				Sorting: []model.Sorting{
					{
						OrderBy: sorting,
						Desc:    true,
					},
				},
			},
		}, nil)

		resp, err := fx.service.List(fx.ctx, &pb.ListRequest{
			Paging: &pb.ListRequest_PagingParams{
				Page:     page,
				PageSize: pageSize,
			},
			Sorting: []*pb.ListRequest_SortingParams{
				{
					OrderBy: pb.SortOrder_SORT_ORDER_BY_PRICE,
					Desc:    true,
				},
			},
		})
		require.NoError(t, err)
		require.NotNil(t, resp)
		require.Len(t, resp.Products, 1)
		require.Equal(t, uint64(100), resp.Total)
		require.NotNil(t, resp.Next)

		require.Equal(t, product.Name, resp.Products[0].Name)
		require.Equal(t, product.Price, resp.Products[0].Price)
		require.Equal(t, product.UpdatesCount, resp.Products[0].UpdatesCount)
		require.Equal(t, product.UpdatedAt, resp.Products[0].UpdatedAt.Seconds)

		require.Equal(t, page+1, resp.Next.Paging.Page)
		require.Equal(t, pageSize, resp.Next.Paging.PageSize)
		require.Len(t, resp.Next.Sorting, 1)
		require.Equal(t, pb.SortOrder_SORT_ORDER_BY_PRICE, resp.Next.Sorting[0].OrderBy)
		require.True(t, resp.Next.Sorting[0].Desc)
	})

	t.Run("list with storage failure", func(t *testing.T) {
		fx := tearUp(t)

		expErr := assert.AnError
		fx.storage.On("GetProductsPage", mock.Anything, mock.Anything).Return(model.ProductsListResponse{}, expErr)

		_, err := fx.service.List(fx.ctx, &pb.ListRequest{})
		require.Error(t, err)
		require.Equal(t, expErr, err)
	})
}
