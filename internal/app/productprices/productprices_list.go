package productprices

import (
	"context"

	"github.com/gogo/protobuf/types"
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/log"

	"bitbucket.org/s2323/productprices/internal/pkg/model"
	pb "bitbucket.org/s2323/productprices/pkg/pb/productprices"
)

// List - возвращает постраничный список продуктов.
func (i *Implementation) List(ctx context.Context, request *pb.ListRequest) (*pb.ListResponse, error) {
	span := opentracing.SpanFromContext(ctx)
	if span == nil {
		span, ctx = opentracing.StartSpanFromContext(ctx, "/productprices.ProductPrices/List")
		defer span.Finish()
	}
	span.LogFields(log.Object("request", request))

	products, err := i.storage.GetProductsPage(ctx, requestToModel(request))
	if err != nil {
		return nil, err
	}
	resp := modelToResponse(products)
	span.LogFields(log.Object("next", resp.Next))
	return resp, nil
}

func requestToModel(request *pb.ListRequest) model.ProductsListRequest {
	result := model.ProductsListRequest{
		Paging: model.Paging{
			Page:     request.GetPaging().GetPage(),
			PageSize: request.GetPaging().GetPageSize(),
		},
		Sorting: make([]model.Sorting, 0, len(request.GetSorting())),
	}
	for _, sorting := range request.GetSorting() {
		result.Sorting = append(result.Sorting, model.Sorting{
			OrderBy: model.SortOrder(sorting.OrderBy),
			Desc:    sorting.Desc,
		})
	}
	return result
}

func modelToResponse(response model.ProductsListResponse) *pb.ListResponse {
	result := &pb.ListResponse{
		Products: make([]*pb.ListResponse_Product, len(response.Products)),
		Total:    uint64(response.Total),
	}
	if response.Next != nil {
		result.Next = &pb.ListRequest{
			Paging: &pb.ListRequest_PagingParams{
				Page:     response.Next.Paging.Page,
				PageSize: response.Next.Paging.PageSize,
			},
			Sorting: make([]*pb.ListRequest_SortingParams, len(response.Next.Sorting)),
		}
		for i, sorting := range response.Next.Sorting {
			result.Next.Sorting[i] = &pb.ListRequest_SortingParams{
				OrderBy: pb.SortOrder(sorting.OrderBy),
				Desc:    sorting.Desc,
			}
		}
	}
	for i, product := range response.Products {
		result.Products[i] = &pb.ListResponse_Product{
			Name:         product.Name,
			Price:        product.Price,
			UpdatedAt:    &types.Timestamp{Seconds: product.UpdatedAt},
			UpdatesCount: product.UpdatesCount,
		}
	}
	return result
}
