package productprices

import (
	"context"
	"net/url"

	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/log"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	pb "bitbucket.org/s2323/productprices/pkg/pb/productprices"
)

// Fetch - запрашивает внешний CSV-файл со списком продуктов по внешнему адресу.
func (i *Implementation) Fetch(ctx context.Context, request *pb.FetchRequest) (*pb.FetchResponse, error) {
	if span := opentracing.SpanFromContext(ctx); span != nil {
		span.LogFields(log.String("URL", request.GetURL()))
	}

	_, err := url.Parse(request.GetURL())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	err = i.queue.Push(ctx, request.URL)
	if err != nil {
		return nil, err
	}
	return &pb.FetchResponse{}, nil
}
