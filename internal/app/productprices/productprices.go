//go:generate mockery -case=underscore -name Storage
//go:generate mockery -case=underscore -name Queue

package productprices

import (
	"context"

	"github.com/go-openapi/spec"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc"

	"bitbucket.org/s2323/productprices/internal/pkg/model"
	"bitbucket.org/s2323/productprices/internal/pkg/platform"
	pb "bitbucket.org/s2323/productprices/pkg/pb/productprices"
	"bitbucket.org/s2323/productprices/pkg/pb/productprices/statik"
)

// Queue - асинхронная очередь загрузки и обработки CSV-файлов
type Queue interface {
	Push(ctx context.Context, url string) error
}

// Storage - хранилище продуктов с возможностью постраничного извлечения информации.
type Storage interface {
	GetProductsPage(ctx context.Context, request model.ProductsListRequest) (model.ProductsListResponse, error)
}

// Implementation реализует интерфейс grpc-сервиса.
type Implementation struct {
	queue   Queue
	storage Storage
}

var _ pb.ProductPricesServer = &Implementation{}

// New создаёт новую реализацию grpc-сервиса.
func New(queue Queue, storage Storage) *Implementation {
	return &Implementation{
		queue:   queue,
		storage: storage,
	}
}

// RegisterGRPC - регистрирует grpc-сервис.
func (i *Implementation) RegisterGRPC(_ context.Context, s *grpc.Server) {
	pb.RegisterProductPricesServer(s, i)
}

// RegisterHTTP - регистрирует http-сервис.
func (i *Implementation) RegisterHTTP(ctx context.Context, mux *runtime.ServeMux) error {
	return pb.RegisterProductPricesHandlerServer(ctx, mux, i)
}

// GetSwagger - возвращает описание сервиса.
func (i *Implementation) GetSwagger() (*spec.Swagger, error) {
	return platform.GetSwagger(statik.Swagger, "/productprices.swagger.json")
}
