package config

import (
	"encoding/json"
	"time"

	secrets "github.com/ijustfool/docker-secrets"
	"github.com/kelseyhightower/envconfig"
	"github.com/mitchellh/mapstructure"
)

// Config - конфигурация сервиса.
type Config struct {
	MongoDatabase string `envconfig:"MONGO_DATABASE" default:"admin"`
	MongoUsername string `envconfig:"MONGO_USERNAME" default:"root"`
	MongoPassword string `envconfig:"MONGO_PASSWORD" mapstructure:"mongo_password" json:"-"`
	MongoHost     string `envconfig:"MONGO_HOST" default:"127.0.0.1"`
	MongoPort     int    `envconfig:"MONGO_PORT" default:"27017"`

	// MaxTryCount - максимально допустимое количество попыток загрузить и обработать CSV-файл.
	MaxTryCount int `envconfig:"MAX_TRY_COUNT" default:"5"`

	// QueueTimeout - время, через которое элемент очереди с статусом InProgress повторно берётся в обработку.
	QueueTimeout time.Duration `envconfig:"QUEUE_TIMEOUT" default:"1h"`

	// WorkersCount - колиество воркеров, параллельно загружающих и обрабатывающих CSV-файлы.
	WorkersCount int `envconfig:"WORKERS_COUNT" default:"1"`
}

// GetConfig - создаёт конфиг на основе переменных окружения  префиксом PRODUCTPRICES_ и docker secrets.
func GetConfig() (*Config, error) {
	cfg := &Config{}
	err := envconfig.Process("PRODUCTPRICES", cfg)
	if err != nil {
		return nil, err
	}

	err = updateFromDockerSecrets(cfg)
	if err != nil {
		return nil, err
	}
	return cfg, nil
}

func updateFromDockerSecrets(cfg *Config) error {
	dockerSecrets, err := secrets.NewDockerSecrets("")
	if err != nil {
		return nil
	}
	return mapstructure.Decode(dockerSecrets.GetAll(), cfg)
}

// String - генерирует строковое представление конфигурации.
func (c *Config) String() string {
	data, err := json.Marshal(c)
	if err != nil {
		return ""
	}
	return string(data)
}
