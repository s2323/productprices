package mongo

import (
	"context"
	"net"
	"strconv"
	"strings"
	"time"

	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/log"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readconcern"
	"go.mongodb.org/mongo-driver/mongo/writeconcern"

	"bitbucket.org/s2323/productprices/internal/config"
	"bitbucket.org/s2323/productprices/internal/pkg/database"
	"bitbucket.org/s2323/productprices/internal/pkg/model"
	"bitbucket.org/s2323/productprices/internal/pkg/platform/buildinfo"
)

const (
	collectionQueue         = "queue"
	collectionProductPrices = "product_prices"
)

// DB - для работы с хранилищем MongoDB.
type DB struct {
	conf     *config.Config
	client   *mongo.Client
	mdb      *mongo.Database
	queue    *mongo.Collection
	products *mongo.Collection
}

// New - создаёт новый объект DB.
func New(ctx context.Context, conf *config.Config) (*DB, error) {
	opts := options.Client().SetAuth(options.Credential{
		Username: conf.MongoUsername,
		Password: conf.MongoPassword,
	}).SetAppName(buildinfo.AppName)

	var hosts []string
	for _, host := range strings.Split(conf.MongoHost, ",") {
		host = net.JoinHostPort(strings.TrimSpace(host), strconv.Itoa(conf.MongoPort))
		hosts = append(hosts, host)
	}
	opts = opts.SetHosts(hosts)

	client, err := mongo.Connect(ctx, opts)
	if err != nil {
		return nil, err
	}

	mdb := client.Database(conf.MongoDatabase)
	queue := mdb.Collection(collectionQueue, options.Collection().
		SetReadConcern(readconcern.Majority()).
		SetWriteConcern(writeconcern.New(writeconcern.WMajority())),
	)
	products := mdb.Collection(collectionProductPrices)

	return &DB{
		conf:     conf,
		client:   client,
		mdb:      mdb,
		queue:    queue,
		products: products,
	}, nil
}

// Close - закрывает соединение с базой данных.
func (db *DB) Close() error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	return db.client.Disconnect(ctx)
}

// QueuePut - добавляет новый (обновляет существующий) элемент в очереди.
func (db *DB) QueuePut(ctx context.Context, element model.QueueElement) error {
	span, ctx := opentracing.StartSpanFromContext(ctx, "database::QueuePut")
	defer span.Finish()

	span.LogFields(log.Object("queue_element", element))

	if len(element.URL) == 0 {
		return nil
	}

	filter := bson.M{
		"url": element.URL,
	}
	update := bson.M{
		"$set": bson.M{
			"url":   element.URL,
			"state": element.State,
			"error": element.Error,
			"ts":    time.Now().Unix(),
		},
	}
	if element.State == model.ProcessingStateNotStarted {
		update = bson.M{
			"$set": bson.M{
				"url":     element.URL,
				"state":   element.State,
				"error":   "",
				"try_cnt": 0,
				"ts":      time.Now().Unix(),
			},
		}
	}
	_, err := db.queue.UpdateOne(ctx, filter, update, options.Update().SetUpsert(true))
	return err
}

// QueueGet - извлекает элемент из очереди на обработку.
func (db *DB) QueueGet(ctx context.Context) (element model.QueueElement, err error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "database::QueueGet")
	defer span.Finish()

	update := bson.M{
		"$set": bson.M{
			"state": model.ProcessingStateInProgress,
			"ts":    time.Now().Unix(),
		},
		"$inc": bson.M{"try_cnt": 1},
	}
	filter := bson.M{"$or": bson.A{
		bson.M{"state": model.ProcessingStateNotStarted},
		bson.M{
			"state":   bson.M{"$eq": model.ProcessingStateFailed},
			"try_cnt": bson.M{"$lt": db.conf.MaxTryCount},
		},
		bson.M{
			"state":   bson.M{"$eq": model.ProcessingStateInProgress},
			"try_cnt": bson.M{"$lt": db.conf.MaxTryCount},
			"ts":      bson.M{"$lte": time.Now().Add(-db.conf.QueueTimeout).Unix()},
		},
	}}
	result := db.queue.FindOneAndUpdate(ctx, filter, update, options.FindOneAndUpdate().SetReturnDocument(options.After))

	if err = result.Err(); err != nil {
		if err == mongo.ErrNoDocuments {
			err = database.ErrNotFound
		}
		return
	}
	err = result.Decode(&element)
	if err == nil {
		span.LogFields(log.Object("queue_element", element))
	}
	return
}

// ProductsPut - сохраняет пачку продуктов с их ценами и датой запроса. При изменении цены инкрементирует счётчик изменений.
func (db *DB) ProductsPut(ctx context.Context, batch []model.Product) error {
	span, ctx := opentracing.StartSpanFromContext(ctx, "database::ProductsPut")
	defer span.Finish()

	span.LogFields(log.Object("batch_size", len(batch)))

	if len(batch) == 0 {
		return nil
	}

	writeModels := make([]mongo.WriteModel, 0, 2*len(batch))
	for _, product := range batch {
		if len(product.NormalizedName) == 0 {
			continue
		}
		writeModel := mongo.NewUpdateOneModel().
			SetFilter(bson.M{
				"n_name": product.NormalizedName,
				"price":  bson.M{"$ne": product.Price},
			}).
			SetUpdate(bson.M{
				"$inc": bson.M{"cnt": 1},
			})
		writeModels = append(writeModels, writeModel)

		writeModel = mongo.NewUpdateOneModel().
			SetFilter(bson.M{
				"n_name": product.NormalizedName,
			}).
			SetUpdate(bson.M{
				"$set": bson.M{
					"n_name": product.NormalizedName,
					"name":   product.Name,
					"price":  product.Price,
					"ts":     product.UpdatedAt,
				},
			}).
			SetUpsert(true)
		writeModels = append(writeModels, writeModel)
	}
	_, err := db.products.BulkWrite(ctx, writeModels)
	return err
}

// GetProductsPage - осуществляет постраничный вывод информации о продуктах.
func (db *DB) GetProductsPage(ctx context.Context, request model.ProductsListRequest) (response model.ProductsListResponse, err error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "database::GetProductsPage")
	defer span.Finish()

	response.Total, err = db.products.CountDocuments(ctx, bson.D{})
	if err != nil {
		return
	}

	opts := request.ToFindOptions(options.Find())
	opts = opts.SetBatchSize(int32(request.Paging.PageSize) + 1)
	if response.Total > 250_000 {
		opts = opts.SetAllowDiskUse(true)
	}

	cursor, err := db.products.Find(ctx, bson.D{}, opts)
	if err != nil {
		return
	}
	defer cursor.Close(ctx)

	response.Products = make([]model.Product, 0, request.Paging.PageSize)
	for i := 0; i < int(request.Paging.PageSize); i++ {
		if !cursor.Next(ctx) {
			break
		}

		var product model.Product
		if err = cursor.Decode(&product); err != nil {
			return
		}
		response.Products = append(response.Products, product)
	}
	if cursor.RemainingBatchLength() > 0 && cursor.Err() == nil {
		response.Next = &request
		response.Next.Paging.Page++
	}
	span.LogFields(log.Object("page_size", len(response.Products)))
	return
}
