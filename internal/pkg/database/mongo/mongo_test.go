package mongo

import (
	"context"
	"testing"
	"time"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database"
	"github.com/golang-migrate/migrate/v4/database/mongodb"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/posener/ctxutil"
	"github.com/stretchr/testify/require"

	"bitbucket.org/s2323/productprices/internal/config"
	internaldb "bitbucket.org/s2323/productprices/internal/pkg/database"
	"bitbucket.org/s2323/productprices/internal/pkg/model"
)

type fixture struct {
	t      *testing.T
	ctx    context.Context
	conf   *config.Config
	mdb    *DB
	driver database.Driver
	m      *migrate.Migrate
}

func tearUp(t *testing.T) *fixture {
	fx := &fixture{
		t:   t,
		ctx: ctxutil.Interrupt(),
	}

	var err error
	fx.conf, err = config.GetConfig()
	require.NoError(t, err)

	mdb, err := New(fx.ctx, fx.conf)
	require.NoError(t, err)
	for i := 0; i < 10; i++ {
		ctx, cancel := context.WithTimeout(fx.ctx, time.Second)
		err = mdb.mdb.Client().Ping(ctx, nil)
		cancel()
		if err == nil {
			break
		}
		t.Logf("waiting for MongoDB to start up... %d", i)
	}
	require.NoError(t, err)

	fx.mdb = mdb
	fx.driver, err = mongodb.WithInstance(fx.mdb.client, &mongodb.Config{
		DatabaseName: fx.conf.MongoDatabase,
	})
	require.NoError(t, err)

	fx.m, err = migrate.NewWithDatabaseInstance("file://./migrations", fx.conf.MongoDatabase, fx.driver)
	require.NoError(t, err)

	err = fx.m.Up()
	if err != migrate.ErrNoChange {
		require.NoError(t, err)
	}

	t.Cleanup(func() {
		err := fx.m.Down()
		if err != migrate.ErrNoChange {
			require.NoError(t, err)
		}

		err = fx.mdb.client.Disconnect(fx.ctx)
		require.NoError(t, err)
	})
	return fx
}

func TestDB_QueuePut(t *testing.T) {
	url := "http://test/test.csv"

	t.Run("empty put ok", func(t *testing.T) {
		fx := tearUp(t)

		err := fx.mdb.QueuePut(fx.ctx, model.QueueElement{})
		require.NoError(t, err)
	})

	t.Run("first put ok", func(t *testing.T) {
		fx := tearUp(t)

		err := fx.mdb.QueuePut(fx.ctx, model.QueueElement{URL: url})
		require.NoError(t, err)
	})

	t.Run("second put ok", func(t *testing.T) {
		fx := tearUp(t)

		err := fx.mdb.QueuePut(fx.ctx, model.QueueElement{URL: url})
		require.NoError(t, err)

		err = fx.mdb.QueuePut(fx.ctx, model.QueueElement{
			URL:       url,
			Error:     "test error",
			State:     model.ProcessingStateFailed,
			TryCount:  42,
			UpdatedAt: time.Now().Unix(),
		})
		require.NoError(t, err)
	})
}

func TestDB_QueueGet(t *testing.T) {
	url := "http://test/test.csv"

	t.Run("empty get ok", func(t *testing.T) {
		fx := tearUp(t)

		_, err := fx.mdb.QueueGet(fx.ctx)
		require.Error(t, err)
		require.Equal(t, internaldb.ErrNotFound, err)
	})

	t.Run("put and get ok", func(t *testing.T) {
		fx := tearUp(t)

		err := fx.mdb.QueuePut(fx.ctx, model.QueueElement{URL: url})
		require.NoError(t, err)

		element, err := fx.mdb.QueueGet(fx.ctx)
		require.NoError(t, err)
		require.Equal(t, url, element.URL)
		require.Equal(t, model.ProcessingStateInProgress, element.State)
		require.NotZero(t, element.UpdatedAt)
		require.NotZero(t, element.TryCount)
		require.Empty(t, element.Error)
	})

	t.Run("get after update is empty", func(t *testing.T) {
		fx := tearUp(t)
		fx.conf.MaxTryCount = 1

		err := fx.mdb.QueuePut(fx.ctx, model.QueueElement{URL: url})
		require.NoError(t, err)

		element, err := fx.mdb.QueueGet(fx.ctx)
		require.NoError(t, err)

		element.State = model.ProcessingStateFailed
		element.Error = "test error"
		err = fx.mdb.QueuePut(fx.ctx, element)
		require.NoError(t, err)

		_, err = fx.mdb.QueueGet(fx.ctx)
		require.Error(t, err)
		require.Equal(t, internaldb.ErrNotFound, err)
	})

	t.Run("get after timeout ok", func(t *testing.T) {
		fx := tearUp(t)
		fx.conf.MaxTryCount = 2
		fx.conf.QueueTimeout = 2 * time.Second

		err := fx.mdb.QueuePut(fx.ctx, model.QueueElement{URL: url})
		require.NoError(t, err)

		_, err = fx.mdb.QueueGet(fx.ctx)
		require.NoError(t, err)

		_, err = fx.mdb.QueueGet(fx.ctx)
		require.Error(t, err)
		require.Equal(t, internaldb.ErrNotFound, err)

		time.Sleep(fx.conf.QueueTimeout)

		_, err = fx.mdb.QueueGet(fx.ctx)
		require.NoError(t, err)
	})

	t.Run("get after success empty", func(t *testing.T) {
		fx := tearUp(t)
		fx.conf.MaxTryCount = 2
		fx.conf.QueueTimeout = 0

		err := fx.mdb.QueuePut(fx.ctx, model.QueueElement{URL: url})
		require.NoError(t, err)

		element, err := fx.mdb.QueueGet(fx.ctx)
		require.NoError(t, err)

		element.State = model.ProcessingStatePassed
		err = fx.mdb.QueuePut(fx.ctx, element)
		require.NoError(t, err)

		_, err = fx.mdb.QueueGet(fx.ctx)
		require.Error(t, err)
		require.Equal(t, internaldb.ErrNotFound, err)
	})
}

func TestDB_ProductsPut(t *testing.T) {
	t.Run("empty put ok", func(t *testing.T) {
		fx := tearUp(t)

		err := fx.mdb.ProductsPut(fx.ctx, nil)
		require.NoError(t, err)
	})

	t.Run("put ok", func(t *testing.T) {
		fx := tearUp(t)

		err := fx.mdb.ProductsPut(fx.ctx, []model.Product{
			{
				NormalizedName: "test product",
				Name:           "test product",
				Price:          66.99,
				UpdatedAt:      time.Now().Unix(),
			},
		})
		require.NoError(t, err)
	})
}

func TestDB_GetProductsPage(t *testing.T) {
	product := model.Product{
		NormalizedName: "test product",
		Name:           "test product",
		Price:          66.99,
		UpdatedAt:      time.Now().Unix(),
	}

	t.Run("empty get ok", func(t *testing.T) {
		fx := tearUp(t)

		resp, err := fx.mdb.GetProductsPage(fx.ctx, model.ProductsListRequest{})
		require.NoError(t, err)
		require.Empty(t, resp.Products)
		require.Zero(t, resp.Total)
		require.Nil(t, resp.Next)
	})

	t.Run("get ok", func(t *testing.T) {
		fx := tearUp(t)

		err := fx.mdb.ProductsPut(fx.ctx, []model.Product{product})
		require.NoError(t, err)

		resp, err := fx.mdb.GetProductsPage(fx.ctx, model.ProductsListRequest{})
		require.NoError(t, err)
		require.Len(t, resp.Products, 1)
		require.Equal(t, int64(1), resp.Total)
		require.Nil(t, resp.Next)
		require.Equal(t, product.NormalizedName, resp.Products[0].NormalizedName)
		require.Equal(t, product.Name, resp.Products[0].Name)
		require.Equal(t, product.Price, resp.Products[0].Price)
		require.Equal(t, product.UpdatedAt, resp.Products[0].UpdatedAt)
		require.Zero(t, resp.Products[0].UpdatesCount)
	})

	t.Run("get after update with the same price ok", func(t *testing.T) {
		fx := tearUp(t)

		err := fx.mdb.ProductsPut(fx.ctx, []model.Product{product})
		require.NoError(t, err)

		err = fx.mdb.ProductsPut(fx.ctx, []model.Product{product})
		require.NoError(t, err)

		resp, err := fx.mdb.GetProductsPage(fx.ctx, model.ProductsListRequest{})
		require.NoError(t, err)
		require.Equal(t, product, resp.Products[0])
	})

	t.Run("get after update with new price ok", func(t *testing.T) {
		fx := tearUp(t)

		err := fx.mdb.ProductsPut(fx.ctx, []model.Product{product})
		require.NoError(t, err)

		update := product
		update.Price += 100
		update.UpdatedAt = time.Now().Unix()
		err = fx.mdb.ProductsPut(fx.ctx, []model.Product{update})
		require.NoError(t, err)

		resp, err := fx.mdb.GetProductsPage(fx.ctx, model.ProductsListRequest{})
		require.NoError(t, err)
		require.Len(t, resp.Products, 1)
		require.Equal(t, int64(1), resp.Total)
		require.Nil(t, resp.Next)

		update.UpdatesCount += 1
		require.Equal(t, update, resp.Products[0])
	})

	t.Run("get with pagination ok", func(t *testing.T) {
		fx := tearUp(t)

		secondProduct := product
		secondProduct.NormalizedName += "#2"
		secondProduct.Name += "#2"

		err := fx.mdb.ProductsPut(fx.ctx, []model.Product{product, secondProduct})
		require.NoError(t, err)

		resp, err := fx.mdb.GetProductsPage(fx.ctx, model.ProductsListRequest{
			Paging: model.Paging{Page: 1, PageSize: 1},
		})
		require.NoError(t, err)
		require.Len(t, resp.Products, 1)
		require.Equal(t, int64(2), resp.Total)
		require.NotNil(t, resp.Next)
		require.Equal(t, product, resp.Products[0])
		require.Equal(t, uint32(2), resp.Next.Paging.Page)

		resp, err = fx.mdb.GetProductsPage(fx.ctx, model.ProductsListRequest{
			Paging: model.Paging{Page: 2, PageSize: 1},
		})
		require.NoError(t, err)
		require.Len(t, resp.Products, 1)
		require.Equal(t, int64(2), resp.Total)
		require.Nil(t, resp.Next)
		require.Equal(t, secondProduct, resp.Products[0])
	})

	t.Run("get with order by price desc ok", func(t *testing.T) {
		fx := tearUp(t)

		secondProduct := product
		secondProduct.NormalizedName += "#2"
		secondProduct.Name += "#2"
		secondProduct.Price += 100

		err := fx.mdb.ProductsPut(fx.ctx, []model.Product{product, secondProduct})
		require.NoError(t, err)

		resp, err := fx.mdb.GetProductsPage(fx.ctx, model.ProductsListRequest{
			Paging: model.Paging{Page: 1, PageSize: 2},
			Sorting: []model.Sorting{
				{
					OrderBy: model.SortOrderByPrice,
					Desc:    true,
				},
			},
		})
		require.NoError(t, err)
		require.Len(t, resp.Products, 2)
		require.Equal(t, int64(2), resp.Total)
		require.Nil(t, resp.Next)
		require.Equal(t, secondProduct, resp.Products[0])
		require.Equal(t, product, resp.Products[1])
	})

	t.Run("get with order by price and name desc ok", func(t *testing.T) {
		fx := tearUp(t)

		secondProduct := product
		secondProduct.NormalizedName += "#2"
		secondProduct.Name += "#2"

		err := fx.mdb.ProductsPut(fx.ctx, []model.Product{product, secondProduct})
		require.NoError(t, err)

		resp, err := fx.mdb.GetProductsPage(fx.ctx, model.ProductsListRequest{
			Paging: model.Paging{Page: 1, PageSize: 2},
			Sorting: []model.Sorting{
				{
					OrderBy: model.SortOrderByPrice,
					Desc:    true,
				},
				{
					OrderBy: model.SortOrderByName,
					Desc:    true,
				},
			},
		})
		require.NoError(t, err)
		require.Len(t, resp.Products, 2)
		require.Equal(t, int64(2), resp.Total)
		require.Nil(t, resp.Next)
		require.Equal(t, secondProduct, resp.Products[0])
		require.Equal(t, product, resp.Products[1])
	})
}
