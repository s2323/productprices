package database

import (
	"github.com/pkg/errors"
)

var (
	// ErrNotFound - элемент отсутствует в хранилище.
	ErrNotFound = errors.New("not found")
)
