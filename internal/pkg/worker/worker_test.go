package worker

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/posener/ctxutil"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap"

	"bitbucket.org/s2323/productprices/internal/pkg/model"
	"bitbucket.org/s2323/productprices/internal/pkg/worker/mocks"
)

type fixture struct {
	t       *testing.T
	ctx     context.Context
	storage *mocks.Storage
	worker  *Worker
}

func tearUp(t *testing.T) *fixture {
	fx := &fixture{
		t:       t,
		ctx:     ctxutil.Interrupt(),
		storage: &mocks.Storage{},
	}
	fx.worker = New(fx.storage)
	fx.worker.logger = zap.NewNop()

	fx.storage.Test(t)
	t.Cleanup(func() {
		fx.storage.AssertExpectations(t)
	})
	return fx
}

func TestWorker_Process(t *testing.T) {
	s := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodGet {
			w.WriteHeader(http.StatusMethodNotAllowed)
			return
		}

		switch r.RequestURI {
		case "/redirect":
			http.Redirect(w, r, "/test.csv", http.StatusMovedPermanently)
		case "/test.csv":
			w.Header().Set("Content-Type", "text/csv")
			w.Write([]byte("xxx;100\nyyy;200\nzzz;300"))
		case "/test_with_header.csv":
			w.Header().Set("Content-Type", "text/csv")
			w.Write([]byte("PRODUCT NAME;PRICE\nxxx;100\nyyy;200\nzzz;300"))
		}
	}))
	defer s.Close()

	expected := []model.Product{
		{
			NormalizedName: "xxx",
			Name:           "xxx",
			Price:          100,
		},
		{
			NormalizedName: "yyy",
			Name:           "yyy",
			Price:          200,
		},
		{
			NormalizedName: "zzz",
			Name:           "zzz",
			Price:          300,
		},
	}

	for name, url := range map[string]string{
		"redirect ok":        fmt.Sprintf("%s/redirect", s.URL),
		"csv no header ok":   fmt.Sprintf("%s/test.csv", s.URL),
		"csv with header ok": fmt.Sprintf("%s/test_with_header.csv", s.URL),
	} {
		t.Run(name, func(t *testing.T) {
			fx := tearUp(t)

			fx.storage.On("ProductsPut", mock.Anything, mock.Anything).Return(func(_ context.Context, batch []model.Product) error {
				require.Len(t, batch, len(expected))
				for i := range batch {
					require.NotZero(t, batch[i].UpdatedAt)
					require.Zero(t, batch[i].UpdatesCount)
					require.Equal(t, expected[i].NormalizedName, batch[i].NormalizedName)
					require.Equal(t, expected[i].Name, batch[i].Name)
					require.Equal(t, expected[i].Price, batch[i].Price)
				}
				return nil
			})

			err := fx.worker.Process(fx.ctx, url)
			require.NoError(t, err)
		})
	}

	t.Run("unsupported url fails", func(t *testing.T) {
		fx := tearUp(t)

		err := fx.worker.Process(fx.ctx, "bad_url")
		require.Error(t, err)
	})

	t.Run("not available", func(t *testing.T) {
		fx := tearUp(t)

		err := fx.worker.Process(fx.ctx, "http://test/test.csv")
		require.Error(t, err)
	})
}
