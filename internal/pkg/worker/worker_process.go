package worker

import (
	"context"
	"encoding/csv"
	"io"
	"strconv"
	"strings"

	"github.com/pkg/errors"
	"go.uber.org/zap"

	"bitbucket.org/s2323/productprices/internal/pkg/model"
)

const (
	comma rune = ';'

	batchSize = 100
)

func (w *Worker) processCSV(ctx context.Context, resp io.ReadCloser, updatedAt int64) error {
	defer resp.Close()

	r := csv.NewReader(resp)
	r.Comma = comma

	batch := make([]model.Product, 0, batchSize)
	for {
		record, err := r.Read()
		if record == nil && err == io.EOF {
			break
		}
		if err != nil {
			return err
		}

		product, err := parseProduct(record)
		if err != nil {
			w.logger.Info("failed to parse product: possible header string", zap.String("csv_record", strings.Join(record, string(comma))), zap.Error(err))
			continue
		}
		product.UpdatedAt = updatedAt

		batch = append(batch, product)
		if len(batch) == batchSize {
			err = w.storage.ProductsPut(ctx, batch)
			if err != nil {
				return err
			}
			batch = batch[0:0]
		}
	}
	if len(batch) > 0 {
		return w.storage.ProductsPut(ctx, batch)
	}
	return nil
}

func parseProduct(record []string) (product model.Product, err error) {
	if len(record) != 2 {
		err = errors.Errorf("bad record length: %d", len(record))
		return
	}
	product.Name = strings.TrimSpace(record[0])
	product.NormalizedName = normalize(record[0])
	product.Price, err = strconv.ParseFloat(strings.TrimSpace(record[1]), 64)
	return
}

func normalize(name string) string {
	// TODO other normalizations
	return strings.ToLower(strings.TrimSpace(name))
}
