//go:generate mockery -case=underscore -name Storage

package worker

import (
	"context"
	"net/http"
	"time"

	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/log"
	"go.uber.org/zap"

	"bitbucket.org/s2323/productprices/internal/pkg/model"
	"bitbucket.org/s2323/productprices/internal/pkg/platform"
)

const (
	timeout = 30 * time.Minute
)

// Storage - хранилище, в котором будут сохранены продукты с их ценой, датой обновления и количеством обновлений.
type Storage interface {
	ProductsPut(ctx context.Context, batch []model.Product) error
}

// Worker - загружает CSV-файл с внешнего адреса, парсит его, сохраняет результаты в хранилище.
type Worker struct {
	httpClient *http.Client
	storage    Storage
	logger     *zap.Logger

	//ftpClient *ftp.ServerConn // TODO
}

// New - создаёт новый Worker.
func New(storage Storage) *Worker {
	return &Worker{
		httpClient: http.DefaultClient,
		storage:    storage,
	}
}

// Process - загружает CSV-файл с внешнего адреса, парсит его, сохраняет результаты в хранилище.
func (w *Worker) Process(ctx context.Context, url string) error {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	span, ctx := opentracing.StartSpanFromContext(ctx, "worker::Process")
	defer span.Finish()

	if w.logger == nil {
		w.logger = platform.GetLoggerFromContext(ctx)
	}
	w.logger.Info("worker start processing url", zap.String("url", url))
	span.LogFields(log.String("URL", url))

	now := time.Now().Unix()
	resp, err := w.download(ctx, url)
	if err != nil {
		return err
	}
	return w.processCSV(ctx, resp, now)
}
