package worker

import (
	"context"
	"io"
	"net/http"
	"net/url"

	"github.com/pkg/errors"
)

func (w *Worker) download(ctx context.Context, u string) (io.ReadCloser, error) {
	uu, err := url.Parse(u)
	if err != nil {
		return nil, err
	}

	switch uu.Scheme {
	case "http", "https":
		return w.downloadHTTP(ctx, u)
	case "ftp":
		// TODO
	}
	return nil, errors.Errorf("unsupported URL: %s", u)
}

func (w *Worker) downloadHTTP(ctx context.Context, u string) (io.ReadCloser, error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, u, nil)
	if err != nil {
		return nil, err
	}

	resp, err := w.httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, errors.Errorf("bad response status: %d (%s)", resp.StatusCode, resp.Status)
	}
	return resp.Body, nil
}
