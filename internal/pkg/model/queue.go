package model

// ProcessingState - состояние элемента в очереди обработки CSV-файлов.
type ProcessingState uint8

const (
	// ProcessingStateNotStarted - CSV-файл ожидает обработки.
	ProcessingStateNotStarted ProcessingState = iota
	// ProcessingStateInProgress - CSV-файл в процессе обработки.
	ProcessingStateInProgress
	// ProcessingStatePassed - обработка CSV-файла завершилась успешно.
	ProcessingStatePassed
	// ProcessingStateFailed - обработка CSV-файла завершилась с ошибкой.
	ProcessingStateFailed
)

// QueueElement - элемент очереди обработки CSV-файлов.
type QueueElement struct {
	// URL - внешний адрес CSV-файла.
	URL string `bson:"url"`
	// State - состояние элемента в очереди обработки CSV-файлов.
	State ProcessingState `bson:"state"`
	// TryCount - количество попыток обработать CSV-файл.
	TryCount uint8 `bson:"try_cnt"`
	// Error - текст ошибки, возникшей во время последней попытки обработать CSV-файл.
	Error string `bson:"err"`
	// UpdatedAt - UNIX timestamp последней попытки обработать CSV-файл.
	UpdatedAt int64 `bson:"ts"`
}
