package model

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const defaultPageSize = 20

// Product - продукт с ценой, датой обновления цены и счётчиком изменений цены.
type Product struct {
	// NormalizedName - нормализованное название продукта.
	NormalizedName string `bson:"n_name"`
	// Name - название продукта.
	Name string `bson:"name"`
	// Price - цена продукта.
	Price float64 `bson:"price"`
	// UpdatesCount - количество изменений цены продукта.
	UpdatesCount uint64 `bson:"cnt"`
	// UpdatedAt - UNIX timestamp момента последнего обновления цены.
	UpdatedAt int64 `bson:"ts"`
}

// SortOrder - порядок сортировки.
type SortOrder uint8

const (
	// SortOrderByName - сортровка по названию продукта (значение по умолчанию).
	SortOrderByName SortOrder = iota
	// SortOrderByPrice - сортировка по цене.
	SortOrderByPrice
	// SortOrderByUpdatedAt - сортировка по времени обновления.
	SortOrderByUpdatedAt
	// SortOrderByUpdatesCount - сортировка по количеству обновлений.
	SortOrderByUpdatesCount
)

// Sorting - настройки сортировки.
type Sorting struct {
	// OrderBy - порядок сортировки (поле).
	OrderBy SortOrder
	// Desc - сортировка по убыванию.
	Desc bool
}

// Paging - настройки пагинации.
type Paging struct {
	// Page - номер страницы (начиная с 1).
	Page uint32
	// PageSize - размер страницы (количество продуктов на странице, по умолчанию 20)
	PageSize uint32
}

// ProductsListRequest - запрос на получение постраничного списка продуктов.
type ProductsListRequest struct {
	Paging  Paging
	Sorting []Sorting
}

// ProductsListResponse - результат запроса на получение постраничного списка продуктов.
type ProductsListResponse struct {
	// Products - список продуктов на странице.
	Products []Product
	// Total - общее количество продуктов
	Total int64
	// Next - запрос для следующей страницы (nil если текущая страница была последней)
	Next *ProductsListRequest
}

// ToFindOptions - заполняет FindOptions параметрами пагинации и сортировки.
func (r *ProductsListRequest) ToFindOptions(opts *options.FindOptions) *options.FindOptions {
	if r.Paging.Page == 0 {
		r.Paging.Page = 1
	}
	if r.Paging.PageSize == 0 {
		r.Paging.PageSize = defaultPageSize
	}
	if len(r.Sorting) == 0 {
		r.Sorting = append(r.Sorting, Sorting{
			OrderBy: SortOrderByName,
		})
	}

	sorting := bson.D{}
	for i := range r.Sorting {
		direction := 1
		if r.Sorting[i].Desc {
			direction = -1
		}

		var key string
		switch r.Sorting[i].OrderBy {
		case SortOrderByName:
			key = "name"
		case SortOrderByPrice:
			key = "price"
		case SortOrderByUpdatedAt:
			key = "ts"
		case SortOrderByUpdatesCount:
			key = "cnt"
		default:
			continue
		}
		sorting = append(sorting, bson.E{Key: key, Value: direction})
	}
	return opts.
		SetLimit(int64(r.Paging.PageSize) + 1).
		SetSkip(int64((r.Paging.Page - 1) * r.Paging.PageSize)).
		SetSort(sorting)
}
