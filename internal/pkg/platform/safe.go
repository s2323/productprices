package platform

import (
	"context"

	"go.uber.org/zap"

	"bitbucket.org/s2323/productprices/internal/pkg/platform/logger"
	"bitbucket.org/s2323/productprices/internal/pkg/platform/middleware"
)

// GoSafe - запускает горутину с восстановлением после паники.
func GoSafe(ctx context.Context, fn func(ctx context.Context)) {
	var l *zap.Logger
	applicationContext := getFromContext(ctx)
	if applicationContext != nil {
		l = applicationContext.logger
	} else {
		l = logger.NewLogger()
	}

	go func(ctx context.Context) {
		defer func() {
			if r := recover(); r != nil {
				middleware.ProcessRecoveredPanic(r, l)
			}
		}()
		fn(ctx)
	}(ctx)
}
