package platform

import (
	"io/ioutil"
	"net"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/go-openapi/spec"
	"github.com/rakyll/statik/fs"

	"bitbucket.org/s2323/productprices/internal/pkg/platform/swagger-ui/statik"
)

// MountSwagger - добавляет swagger в отладочный сервер.
func MountSwagger(mux *chi.Mux, service Service) error {
	hfs, err := fs.NewWithNamespace(statik.SwaggerUi)
	if err != nil {
		return err
	}

	sw, err := service.GetSwagger()
	if err != nil {
		return err
	}

	mux.Get("/swagger.json", func(w http.ResponseWriter, r *http.Request) {
		if sw == nil {
			http.NotFound(w, r)
			return
		}

		data, err := fixSwagger(sw, r).MarshalJSON()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(data) //nolint:errcheck,gosec
	})
	mux.Get("/swagger", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/swagger/", http.StatusMovedPermanently)
	})
	mux.Mount("/swagger/", http.StripPrefix("/swagger", http.FileServer(hfs)))
	return nil
}

// GetSwagger - извлекает описание сервиса из statik FS.
func GetSwagger(ns, file string) (*spec.Swagger, error) {
	hfs, err := fs.NewWithNamespace(ns)
	if err != nil {
		return nil, err
	}

	f, err := hfs.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	data, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, err
	}

	sw := &spec.Swagger{}
	if err = sw.UnmarshalJSON(data); err != nil {
		return nil, err
	}
	return sw, nil
}

func fixSwagger(sw *spec.Swagger, r *http.Request) *spec.Swagger {
	if h, _, err := net.SplitHostPort(r.Host); err == nil {
		if ips, err := net.LookupIP(h); err == nil && len(ips) > 0 {
			sw.Host = net.JoinHostPort(ips[0].String(), strconv.FormatInt(httpPort, 10))
		}
	}
	return sw
}
