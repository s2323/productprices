package platform

import (
	"context"
	"encoding/json"
	"net"
	"net/http"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"bitbucket.org/s2323/productprices/internal/pkg/platform/buildinfo"
)

// MountDebug - подключает дополнительные отладочные обработчики.
func MountDebug(ctx context.Context, mux *chi.Mux) {
	mountVersion(ctx, mux)
	mux.Mount("/debug", middleware.Profiler())
	mux.Handle("/metrics", promhttp.Handler())
}

func mountVersion(ctx context.Context, mux *chi.Mux) {
	startTime := time.Now().UTC()
	applicationContext := getFromContext(ctx)
	if applicationContext != nil {
		startTime = applicationContext.startedAt
	}

	mux.Get("/version", func(w http.ResponseWriter, r *http.Request) {
		version := map[string]interface{}{
			"appName":    buildinfo.AppName,
			"appVersion": buildinfo.AppVersion,
			"goVersion":  buildinfo.GoVersion,
			"uptime":     time.Now().UTC().Sub(startTime).String(),
			"host":       myIP(),
		}

		data, err := json.Marshal(version)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(data) //nolint:errcheck,gosec
	})
}

func myIP() string {
	ifaces, err := net.Interfaces()
	if err != nil {
		return err.Error()
	}
	for _, iface := range ifaces {
		if iface.Flags&net.FlagUp == 0 {
			continue // interface down
		}
		if iface.Flags&net.FlagLoopback != 0 {
			continue // loopback interface
		}

		addrs, err := iface.Addrs()
		if err != nil {
			return err.Error()
		}
		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}
			if ip == nil || ip.IsLoopback() {
				continue
			}
			ip = ip.To4()
			if ip == nil {
				continue // not an ipv4 address
			}
			return ip.String()
		}
	}
	return ""
}
