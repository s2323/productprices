package middleware

import (
	"context"
	"net/http"

	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/status"

	"bitbucket.org/s2323/productprices/internal/pkg/platform/logger"
)

type writerWrapper struct {
	w    http.ResponseWriter
	code int
}

// Header - реализует http.ResponseWriter.
func (w *writerWrapper) Header() http.Header {
	return w.w.Header()
}

// Write - реализует http.ResponseWriter.
func (w *writerWrapper) Write(data []byte) (int, error) {
	return w.w.Write(data)
}

// WriteHeader - реализует http.ResponseWriter.
func (w *writerWrapper) WriteHeader(statusCode int) {
	w.code = statusCode
	w.w.WriteHeader(statusCode)
}

// RequestLoggerHTTP - логирует запросы, завершившиеся ошибкой.
func RequestLoggerHTTP(next http.Handler) http.Handler {
	l := logger.NewLogger()
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		wrapper := &writerWrapper{w: w, code: http.StatusOK}
		next.ServeHTTP(wrapper, r)
		if wrapper.code != http.StatusOK {
			l.Error("http request failed",
				zap.Int("status_code", wrapper.code),
				zap.String("request_method", r.Method),
				zap.String("request_uri", r.RequestURI),
			)
		}
	})
}

// RequestLoggerGRPC - логирует запросы, завершившиеся ошибкой.
func RequestLoggerGRPC() grpc.UnaryServerInterceptor {
	l := logger.NewLogger()
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		resp, err = handler(ctx, req)
		if err != nil {
			l.Error("grpc request failed",
				zap.Error(err),
				zap.String("status_code", status.Code(err).String()),
				zap.String("request_method", info.FullMethod),
			)
		}
		return
	}
}
