package middleware

import (
	"context"
	"fmt"
	"net/http"

	"github.com/opentracing/opentracing-go/log"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
)

var spanHeaders = map[string]struct{}{
	"x-b3-traceid":      {},
	"x-b3-parentspanid": {},
	"x-b3-spanid":       {},
	"x-b3-sampled":      {},
}

// TracingHTTP - трейсинг.
func TracingHTTP(next http.Handler) http.Handler {
	tracer := opentracing.GlobalTracer()
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		for hdr := range spanHeaders {
			key := fmt.Sprintf("%s%s", runtime.MetadataHeaderPrefix, hdr)
			for _, value := range r.Header.Values(hdr) {
				r.Header.Add(key, value)
			}
		}

		var span opentracing.Span
		method := fmt.Sprintf("%s %s", r.Method, r.RequestURI)
		carrier := opentracing.HTTPHeadersCarrier(r.Header)
		clientContext, err := tracer.Extract(opentracing.HTTPHeaders, carrier)
		if err == nil {
			span = tracer.StartSpan(method, ext.RPCServerOption(clientContext))
		} else {
			span = tracer.StartSpan(method)
		}
		ctx = opentracing.ContextWithSpan(ctx, span)
		defer span.Finish()

		ext.HTTPMethod.Set(span, r.Method)
		ext.HTTPUrl.Set(span, r.RequestURI)
		ext.PeerAddress.Set(span, r.RemoteAddr)

		wrapper := &writerWrapper{w: w, code: http.StatusOK}
		next.ServeHTTP(w, r.WithContext(ctx))
		ext.HTTPStatusCode.Set(span, uint16(wrapper.code))
		if wrapper.code != http.StatusOK {
			ext.Error.Set(span, true)
		}
	})
}

// TracingGRPC - трейсинг.
func TracingGRPC() grpc.UnaryServerInterceptor {
	tracer := opentracing.GlobalTracer()
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		md, ok := metadata.FromIncomingContext(ctx)
		if !ok {
			md = metadata.New(nil)
		}

		var span opentracing.Span
		carrier := &metadataCarrier{md: md}
		clientContext, err := tracer.Extract(opentracing.HTTPHeaders, carrier)
		if err == nil {
			span = tracer.StartSpan(info.FullMethod, ext.RPCServerOption(clientContext))
		} else {
			span = tracer.StartSpan(info.FullMethod)
		}
		ctx = opentracing.ContextWithSpan(ctx, span)
		defer span.Finish()

		resp, err = handler(ctx, req)
		if err != nil {
			ext.Error.Set(span, true)
			log.Error(err)
		}
		return
	}
}

type metadataCarrier struct {
	md metadata.MD
}

// Set conforms to the TextMapWriter interface.
func (c *metadataCarrier) Set(key, val string) {
	c.md.Append(key, val)
}

// ForeachKey conforms to the TextMapReader interface.
func (c *metadataCarrier) ForeachKey(handler func(key, val string) error) error {
	for k, vals := range c.md {
		for _, v := range vals {
			if err := handler(k, v); err != nil {
				return err
			}
		}
	}
	return nil
}
