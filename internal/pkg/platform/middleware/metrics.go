package middleware

import (
	"context"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/middleware"
	"github.com/prometheus/client_golang/prometheus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/status"

	"bitbucket.org/s2323/productprices/internal/pkg/platform/buildinfo"
)

const ns = "pp"

var (
	httpRequestsCount = prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: ns,
		Subsystem: buildinfo.AppName,
		Name:      "http_requests_count",
		Help:      "HTTP requests count",
	}, []string{"method", "path"})
	httpResponseCount = prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: ns,
		Subsystem: buildinfo.AppName,
		Name:      "http_response_count",
		Help:      "HTTP response counter by status code",
	}, []string{"method", "path", "code"})

	grpcRequestsCount = prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: ns,
		Subsystem: buildinfo.AppName,
		Name:      "grpc_requests_count",
		Help:      "gRPC requests count",
	}, []string{"method"})
	grpcResponseCount = prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: ns,
		Subsystem: buildinfo.AppName,
		Name:      "grpc_response_count",
		Help:      "gRPC response counter by status code",
	}, []string{"method", "code"})
)

func init() {
	prometheus.MustRegister(
		httpRequestsCount,
		httpResponseCount,
		grpcRequestsCount,
		grpcResponseCount,
	)
}

// MetricsHTTP - метрики для мониторинга.
func MetricsHTTP(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		httpRequestsCount.WithLabelValues(r.Method, r.URL.Path).Inc()
		wrapper := middleware.NewWrapResponseWriter(w, 0)
		next.ServeHTTP(wrapper, r)
		code := http.StatusOK
		if wrapper.Status() > 0 {
			code = wrapper.Status()
		}
		httpResponseCount.WithLabelValues(r.Method, r.URL.Path, fmt.Sprintf("%d", code)).Inc()
	})
}

// MetricsGRPC - метрики для мониторинга.
func MetricsGRPC() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		grpcRequestsCount.WithLabelValues(info.FullMethod).Inc()
		defer func() {
			grpcResponseCount.WithLabelValues(info.FullMethod, status.Code(err).String()).Inc()
		}()

		return handler(ctx, req)
	}
}
