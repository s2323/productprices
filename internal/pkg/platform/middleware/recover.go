package middleware

import (
	"context"
	"net/http"
	"runtime/debug"
	"strings"

	"github.com/pkg/errors"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"bitbucket.org/s2323/productprices/internal/pkg/platform/logger"
)

// RecoverHTTP - ловит паники.
func RecoverHTTP(next http.Handler) http.Handler {
	l := logger.NewLogger()
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if p := recover(); p != nil {
				ProcessRecoveredPanic(p, l)
				w.WriteHeader(http.StatusInternalServerError)
			}
		}()
		next.ServeHTTP(w, r)
	})
}

// RecoverGRPC - ловит паники.
func RecoverGRPC() grpc.UnaryServerInterceptor {
	l := logger.NewLogger()
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		defer func() {
			if p := recover(); p != nil {
				ProcessRecoveredPanic(p, l)
				err = status.Error(codes.Internal, "request failed")
			}
		}()
		return handler(ctx, req)
	}
}

// ProcessRecoveredPanic - логирует панику.
func ProcessRecoveredPanic(r interface{}, l *zap.Logger) {
	stack := strings.Split(string(debug.Stack()), "\n")
	l.Error("recovered from panic",
		zap.Strings("stacktrace", stack),
		zap.Error(errors.Errorf("panic: %+v", r)),
	)
}
