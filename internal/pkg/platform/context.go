package platform

import (
	"context"
	"io"
	"sync"
	"time"

	"github.com/posener/ctxutil"
	"go.uber.org/zap"

	"bitbucket.org/s2323/productprices/internal/pkg/platform/buildinfo"
	"bitbucket.org/s2323/productprices/internal/pkg/platform/logger"
)

const (
	shutdownTimeout = 10 * time.Second
)

// ApplicationContext - контекст приложения.
type ApplicationContext struct {
	cancel    context.CancelFunc
	closer    []io.Closer
	logger    *zap.Logger
	startedAt time.Time
	once      *sync.Once
}

type applicationContextKeyType struct{}

var applicationContextKey applicationContextKeyType

func getFromContext(ctx context.Context) *ApplicationContext {
	if v, ok := ctx.Value(applicationContextKey).(*ApplicationContext); ok {
		return v
	}
	return nil
}

// GetApplicationContext - создаёт контекст приложения.
// Все горутины, запущенные приложением и работающие в бэкграунде должны быть остановлены при завершении этого контекста.
func GetApplicationContext() context.Context {
	ctx, cancel := context.WithCancel(ctxutil.Interrupt())
	applicationContext := &ApplicationContext{
		cancel:    cancel,
		logger:    logger.NewLogger(),
		startedAt: time.Now().UTC(),
		once:      &sync.Once{},
	}
	ctx = context.WithValue(ctx, applicationContextKey, applicationContext)

	initJaegerTracer(ctx)
	applicationContext.logger.Info("starting application",
		zap.String("app_name", buildinfo.AppName),
		zap.String("app_version", buildinfo.AppVersion),
		zap.String("go_version", buildinfo.GoVersion),
	)
	return ctx
}

// AppendCloser - добавляет io.Closer в контекст приложения.
// Все объекты io.Closer будут вызваны при завершении приложения в порядке обратном порядку добавления.
func AppendCloser(ctx context.Context, closer io.Closer) {
	applicationContext := getFromContext(ctx)
	if applicationContext == nil {
		return
	}
	applicationContext.closer = append(applicationContext.closer, closer)
}

// CloserFn - функция с сигнатурой io.Closer.
type CloserFn func() error

// Close - реализация интерфейса io.Closer.
func (c CloserFn) Close() error {
	return c()
}

// WaitAndClose - ожидает завершения контекста и вызывает все объекты io.Closer приложения, ранее добавленные с помощью AppendCloser().
func WaitAndClose(ctx context.Context) {
	<-ctx.Done()

	applicationContext := getFromContext(ctx)
	if applicationContext == nil {
		return
	}

	applicationContext.once.Do(func() {
		closeCtx, cancel := context.WithTimeout(context.Background(), shutdownTimeout)
		defer cancel()

		go func(cancel context.CancelFunc) {
			var err error
			for i := len(applicationContext.closer) - 1; i >= 0; i-- {
				if err = applicationContext.closer[i].Close(); err != nil {
					applicationContext.logger.Error("application closer failed", zap.Error(err))
				}
			}
			cancel()
		}(cancel)

		<-closeCtx.Done()
	})
}

// GetLoggerFromContext - возвращает логгер из контекста приложения, либо создаёт новый.
func GetLoggerFromContext(ctx context.Context) *zap.Logger {
	applicationContext := getFromContext(ctx)
	if applicationContext == nil {
		return logger.NewLogger()
	}
	return applicationContext.logger
}
