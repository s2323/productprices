package platform

import (
	"context"
	"fmt"
	"net"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/cors"
	"github.com/go-openapi/spec"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/opentracing/opentracing-go"
	"github.com/uber/jaeger-client-go"
	"github.com/uber/jaeger-client-go/config"
	"github.com/uber/jaeger-lib/metrics/prometheus"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"bitbucket.org/s2323/productprices/internal/pkg/platform/buildinfo"
	"bitbucket.org/s2323/productprices/internal/pkg/platform/middleware"
)

// Service - сервис.
type Service interface {
	RegisterGRPC(ctx context.Context, s *grpc.Server)
	RegisterHTTP(ctx context.Context, mux *runtime.ServeMux) error
	GetSwagger() (*spec.Swagger, error)
	CORS() *cors.Options
}

// Server - сервер.
type Server interface {
	Serve(listener net.Listener) error
}

// StartGRPCServer - запускает grpc-сервер на основе указанного сервиса.
func StartGRPCServer(ctx context.Context, service Service) error {
	s := grpc.NewServer(
		grpc.ChainUnaryInterceptor(
			middleware.RequestLoggerGRPC(),
			middleware.MetricsGRPC(),
			middleware.TracingGRPC(),
			middleware.RecoverGRPC(),
		),
	)
	service.RegisterGRPC(ctx, s)
	reflection.Register(s)
	return runServer(ctx, s, grpcPort)
}

// StartHTTPServer - запускает HTTP-сервер на основе указанного сервиса.
func StartHTTPServer(ctx context.Context, service Service) error {
	mux := runtime.NewServeMux()
	err := service.RegisterHTTP(ctx, mux)
	if err != nil {
		return err
	}

	var mw = chi.Middlewares{
		middleware.RequestLoggerHTTP,
		middleware.MetricsHTTP,
		middleware.TracingHTTP,
		middleware.RecoverHTTP,
	}
	if corsOptions := service.CORS(); corsOptions != nil {
		mw = append(mw, cors.Handler(*corsOptions))
	}
	return runServer(ctx, &http.Server{Handler: chi.Chain(mw...).Handler(mux)}, httpPort)
}

// StartDebugServer - запускает отладочный сервер.
func StartDebugServer(ctx context.Context, service Service) error {
	mux := chi.NewMux()
	err := MountSwagger(mux, service)
	if err != nil {
		return err
	}
	MountDebug(ctx, mux)
	return runServer(ctx, &http.Server{Handler: mux}, debugPort)
}

// Run - запускает GRPC-сервер, HTTP-сервер и отладочный сервер.
func Run(ctx context.Context, service Service) error {
	if err := StartGRPCServer(ctx, service); err != nil {
		return err
	}
	if err := StartHTTPServer(ctx, service); err != nil {
		return err
	}
	if err := StartDebugServer(ctx, service); err != nil {
		return err
	}
	return nil
}

func runServer(ctx context.Context, s Server, port int) error {
	l, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		return err
	}

	AppendCloser(ctx, shutdownServer(ctx, s, port))
	go func(ctx context.Context) {
		if err := s.Serve(l); err != nil && err != http.ErrServerClosed {
			applicationContext := getFromContext(ctx)
			if applicationContext == nil {
				panic(err)
			}
			applicationContext.logger.Error("serve failed", zap.Int("port", port), zap.Error(err))
			applicationContext.cancel()
		}
	}(ctx)
	return nil
}

func shutdownServer(ctx context.Context, s Server, port int) CloserFn {
	return func() error {
		defer func() {
			applicationContext := getFromContext(ctx)
			if applicationContext != nil {
				applicationContext.logger.Info("server closed", zap.Int("port", port))
			}
		}()

		switch v := s.(type) {
		case *http.Server:
			shutdownContext, cancel := context.WithTimeout(context.Background(), shutdownTimeout)
			defer cancel()
			return v.Shutdown(shutdownContext)
		case *grpc.Server:
			v.GracefulStop()
		}
		return nil
	}
}

func initJaegerTracer(ctx context.Context) {
	cfg, err := config.FromEnv()
	if err != nil {
		return
	}
	cfg.ServiceName = buildinfo.AppName

	var logger jaeger.Logger = jaeger.StdLogger
	if applicationContext := getFromContext(ctx); applicationContext != nil {
		logger = jaegerLoggerWrapper{
			logger: applicationContext.logger,
			sugar:  applicationContext.logger.Sugar(),
		}
	}

	tracer, closer, err := cfg.NewTracer(
		config.Logger(logger),
		config.Metrics(prometheus.New()),
	)
	if err == nil {
		opentracing.InitGlobalTracer(tracer)
		AppendCloser(ctx, closer)
	}
}

type jaegerLoggerWrapper struct {
	logger *zap.Logger
	sugar  *zap.SugaredLogger
}

// Error - реализует jaeger.Logger.
func (l jaegerLoggerWrapper) Error(msg string) {
	l.logger.Error(msg)
}

// Infof - реализует jaeger.Logger.
func (l jaegerLoggerWrapper) Infof(msg string, args ...interface{}) {
	l.sugar.Infof(msg, args...)
}
