package queue

import (
	"context"
	"sync"
	"testing"
	"time"

	"github.com/posener/ctxutil"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap"

	"bitbucket.org/s2323/productprices/internal/pkg/database"
	"bitbucket.org/s2323/productprices/internal/pkg/model"
	"bitbucket.org/s2323/productprices/internal/pkg/queue/mocks"
)

type fixture struct {
	t       *testing.T
	ctx     context.Context
	storage *mocks.Storage
	worker  *mocks.Worker
	queue   *Queue
	wg      *sync.WaitGroup
}

func tearUp(t *testing.T) *fixture {
	fx := &fixture{
		t:       t,
		ctx:     ctxutil.Interrupt(),
		storage: &mocks.Storage{},
		worker:  &mocks.Worker{},
		wg:      &sync.WaitGroup{},
	}
	fx.queue = New(fx.storage, fx.worker)
	fx.queue.logger = zap.NewNop()

	fx.storage.Test(t)
	fx.worker.Test(t)
	t.Cleanup(func() {
		fx.wg.Wait()
		fx.storage.AssertExpectations(t)
		fx.worker.AssertExpectations(t)
	})
	return fx
}

func (fx *fixture) wait() {
	fx.wg.Add(1)
	go func() {
		fx.wg.Done()
		fx.queue.m.Lock()
		fx.queue.cv.Wait()
		fx.queue.m.Unlock()
	}()
}

func TestQueue_Push(t *testing.T) {
	url := "http:/test/test.csv"

	t.Run("push ok", func(t *testing.T) {
		fx := tearUp(t)

		fx.storage.On("QueuePut", mock.Anything, model.QueueElement{URL: url}).Return(nil)
		fx.wait()

		err := fx.queue.Push(fx.ctx, url)
		require.NoError(t, err)
	})

	t.Run("push error", func(t *testing.T) {
		fx := tearUp(t)

		expErr := assert.AnError
		fx.storage.On("QueuePut", mock.Anything, model.QueueElement{URL: url}).Return(expErr)

		err := fx.queue.Push(fx.ctx, url)
		require.Error(t, err)
		require.Equal(t, expErr, err)
	})
}

func TestQueue_Process(t *testing.T) {
	url := "http:/test/test.csv"

	t.Run("empty queue ok", func(t *testing.T) {
		fx := tearUp(t)

		ctx, cancel := context.WithCancel(fx.ctx)
		fx.storage.On("QueueGet", mock.Anything).Return(model.QueueElement{}, database.ErrNotFound)
		fx.wait()

		fx.wg.Add(1)
		go func() {
			defer fx.wg.Done()
			time.Sleep(time.Second)
			cancel()
		}()

		fx.queue.Process(ctx)
	})

	t.Run("queue get error", func(t *testing.T) {
		fx := tearUp(t)

		expErr := assert.AnError
		ctx, cancel := context.WithCancel(fx.ctx)
		fx.storage.On("QueueGet", mock.Anything).Return(model.QueueElement{}, func(context.Context) error {
			cancel()
			return expErr
		})
		fx.wait()

		fx.queue.Process(ctx)
	})

	t.Run("worker error", func(t *testing.T) {
		fx := tearUp(t)

		expErr := assert.AnError
		ctx, cancel := context.WithCancel(fx.ctx)
		fx.storage.On("QueueGet", mock.Anything).Return(model.QueueElement{URL: url}, nil)
		fx.worker.On("Process", mock.Anything, url).Return(expErr)
		fx.storage.On("QueuePut", mock.Anything, model.QueueElement{
			URL:   url,
			State: model.ProcessingStateFailed,
			Error: expErr.Error(),
		}).Return(func(context.Context, model.QueueElement) error {
			cancel()
			return nil
		})
		fx.wait()

		fx.queue.Process(ctx)
	})

	t.Run("worker ok", func(t *testing.T) {
		fx := tearUp(t)

		ctx, cancel := context.WithCancel(fx.ctx)
		fx.storage.On("QueueGet", mock.Anything).Return(model.QueueElement{URL: url}, nil)
		fx.worker.On("Process", mock.Anything, url).Return(nil)
		fx.storage.On("QueuePut", mock.Anything, model.QueueElement{
			URL:   url,
			State: model.ProcessingStatePassed,
		}).Return(func(context.Context, model.QueueElement) error {
			cancel()
			return nil
		})
		fx.wait()

		fx.queue.Process(ctx)
	})

	t.Run("queue put error", func(t *testing.T) {
		fx := tearUp(t)

		expErr := assert.AnError
		ctx, cancel := context.WithCancel(fx.ctx)
		fx.storage.On("QueueGet", mock.Anything).Return(model.QueueElement{URL: url}, nil)
		fx.worker.On("Process", mock.Anything, url).Return(nil)
		fx.storage.On("QueuePut", mock.Anything, model.QueueElement{
			URL:   url,
			State: model.ProcessingStatePassed,
		}).Return(func(context.Context, model.QueueElement) error {
			cancel()
			return expErr
		})
		fx.wait()

		fx.queue.Process(ctx)
	})
}
