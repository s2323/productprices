//go:generate mockery -case=underscore -name Storage
//go:generate mockery -case=underscore -name Worker

package queue

import (
	"context"
	"sync"
	"time"

	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"github.com/opentracing/opentracing-go/log"
	"go.uber.org/zap"

	"bitbucket.org/s2323/productprices/internal/pkg/database"
	"bitbucket.org/s2323/productprices/internal/pkg/model"
	"bitbucket.org/s2323/productprices/internal/pkg/platform"
)

const (
	emptyQueueTimeout = 10 * time.Second
)

// Storage - хранилище, на котором реализована очередь внешних адресов CSV-файлов.
type Storage interface {
	QueuePut(ctx context.Context, element model.QueueElement) error
	QueueGet(ctx context.Context) (model.QueueElement, error)
}

// Worker - обработчик CSV-файлов (загружает, парсит, сохраняет и т.п.).
type Worker interface {
	Process(ctx context.Context, url string) error
}

// Queue - очередь для асинхронной загрузки CSV-файлов.
type Queue struct {
	storage Storage
	pool    chan Worker
	logger  *zap.Logger
	cv      *sync.Cond
	m       *sync.Mutex
}

// New создаёт новую очередь.
func New(storage Storage, workers ...Worker) *Queue {
	pool := make(chan Worker, len(workers))
	for i := range workers {
		pool <- workers[i]
	}

	mutex := &sync.Mutex{}
	return &Queue{
		storage: storage,
		pool:    pool,
		cv:      sync.NewCond(mutex),
		m:       mutex,
	}
}

// Push - добавляет внешний адрес CSV-файла в очередь для обработки.
func (q *Queue) Push(ctx context.Context, url string) error {
	err := q.storage.QueuePut(ctx, model.QueueElement{
		URL: url,
	})
	if err != nil {
		return err
	}
	q.cv.Broadcast()
	return nil
}

// Process - запускает цикл обработки очереди.
// Необходимо запускать в отдельной горутине.
// Выход из цикла обработки происходит после завершения контекста.
func (q *Queue) Process(ctx context.Context) {
	if q.logger == nil {
		q.logger = platform.GetLoggerFromContext(ctx)
	}
	ticker := time.NewTicker(emptyQueueTimeout)
	wg := sync.WaitGroup{}
	defer func() {
		ticker.Stop()
		wg.Wait()
	}()

	for {
		select {
		case <-ctx.Done():
			q.cv.Broadcast()
			return
		case <-ticker.C:
			q.cv.Broadcast()
		case w, ok := <-q.pool:
			if !ok {
				return
			}

			wg.Add(1)
			platform.GoSafe(ctx, func(ctx context.Context) {
				defer func() {
					wg.Done()
					q.pool <- w
				}()

				span := opentracing.GlobalTracer().StartSpan("queue::Process")
				ctx = opentracing.ContextWithSpan(ctx, span)
				err := q.process(ctx, w)
				if err == database.ErrNotFound {
					span.LogFields(log.Bool("empty_queue", true))
					span.Finish()
					q.m.Lock()
					q.cv.Wait()
					q.m.Unlock()
					return
				}
				if err != nil {
					q.logger.Error("processing failed", zap.Error(err))
					ext.Error.Set(span, true)
					span.LogFields(log.Error(err))
				}
				span.Finish()
			})
		}
	}
}

func (q *Queue) process(ctx context.Context, w Worker) error {
	element, err := q.storage.QueueGet(ctx)
	if err != nil {
		return err
	}
	element.State = model.ProcessingStatePassed
	element.Error = ""

	err = w.Process(ctx, element.URL)
	if err != nil {
		element.State = model.ProcessingStateFailed
		element.Error = err.Error()
	}
	return q.storage.QueuePut(ctx, element)
}
