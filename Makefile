include scripts/dev/Makefile

.PHONY: protoc-gen
protoc-gen:
	GOBIN=$(CURDIR)/bin go install \
		github.com/golang/protobuf/protoc-gen-go \
		github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2 \
		github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway \
		github.com/rakyll/statik

PROTOM := Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types

.PHONY: generate-proto
generate-proto:
	mkdir -p $(CURDIR)/pkg/pb/ && \
	protoc \
		--plugin=protoc-gen-go=$(CURDIR)/bin/protoc-gen-go \
		--plugin=protoc-gen-openapiv2=$(CURDIR)/bin/protoc-gen-openapiv2 \
		--plugin=protoc-gen-grpc-gateway=$(CURDIR)/bin/protoc-gen-grpc-gateway \
		-I$(CURDIR)/api/proto \
		--go_out=$(PROTOM),plugins=grpc:. \
		--grpc-gateway_out=$(PROTOM):. \
		--openapiv2_out $(CURDIR)/pkg/pb/ \
		$(CURDIR)/api/proto/productprices/productprices.proto
	$(CURDIR)/bin/statik -ns=swagger -include=*.swagger.json -f \
		-dest=$(CURDIR)/pkg/pb/productprices/ \
		-src=$(CURDIR)/pkg/pb/productprices/


GIT_TAG := $(shell git describe --exact-match --exclude "pkg/pb/productprices/*" --abbrev=0 --tags 2> /dev/null)
GIT_BRANCH := $(shell git branch 2> /dev/null | grep '*' | cut -f2 -d' ')
APP_NAME := $(shell basename $(CURDIR))
APP_VERSION := $(if $(GIT_TAG),$(GIT_TAG),$(GIT_BRANCH))
GO_VERSION := $(shell go version)

LDFLAGS := -X 'bitbucket.org/s2323/productprices/internal/pkg/platform/buildinfo.AppName=${APP_NAME}' \
           -X 'bitbucket.org/s2323/productprices/internal/pkg/platform/buildinfo.AppVersion=${APP_VERSION}' \
           -X 'bitbucket.org/s2323/productprices/internal/pkg/platform/buildinfo.GoVersion=${GO_VERSION}'

.PHONY: build
build:
	CGO_ENABLED=0 go build -ldflags "${LDFLAGS}" -o $(CURDIR)/bin/productprices $(CURDIR)/cmd/productprices/main.go
