module bitbucket.org/s2323/productprices

go 1.14

require (
	bitbucket.org/s2323/productprices/pkg/pb/productprices v0.0.0-00010101000000-000000000000
	github.com/HdrHistogram/hdrhistogram-go v0.9.0 // indirect
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-chi/cors v1.1.1
	github.com/go-openapi/spec v0.19.9
	github.com/gogo/protobuf v1.3.1
	github.com/golang-migrate/migrate/v4 v4.13.0
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.0.0
	github.com/ijustfool/docker-secrets v0.0.0-20191021062307-b25ea5007562
	github.com/jlaffaye/ftp v0.0.0-20200812143550-39e3779af0db
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/mitchellh/mapstructure v1.1.2
	github.com/opentracing/opentracing-go v1.1.0
	github.com/pkg/errors v0.9.1
	github.com/posener/ctxutil v1.0.0
	github.com/prometheus/client_golang v1.8.0
	github.com/rakyll/statik v0.1.7
	github.com/stretchr/testify v1.6.1
	github.com/uber/jaeger-client-go v2.25.0+incompatible
	github.com/uber/jaeger-lib v2.4.0+incompatible
	go.mongodb.org/mongo-driver v1.4.2
	go.uber.org/zap v1.13.0
	google.golang.org/grpc v1.33.0
)

replace bitbucket.org/s2323/productprices/pkg/pb/productprices => ./pkg/pb/productprices
