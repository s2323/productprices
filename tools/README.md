# README #

Генерация [statik](https://github.com/rakyll/statik) файла осуществляется следующим образом:

```
make protoc-gen
sed -i 's"https://petstore.swagger.io/v2""' ./tools/swagger-ui-dist/index.html
./bin/statik -ns='swagger-ui' -f -dest=./internal/pkg/platform/swagger-ui -src=./tools/swagger-ui-dist/
```
