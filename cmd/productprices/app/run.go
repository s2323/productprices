package app

import (
	"expvar"

	"bitbucket.org/s2323/productprices/internal/config"
	"bitbucket.org/s2323/productprices/internal/pkg/platform"
)

// Run - создаёт и запускает приложение.
func Run() {
	ctx := platform.GetApplicationContext()

	conf, err := config.GetConfig()
	if err != nil {
		panic(err)
	}
	expvar.Publish("config", conf)

	ppApp, err := New(ctx, conf)
	if err != nil {
		panic(err)
	}
	if err = ppApp.Run(ctx); err != nil {
		panic(err)
	}
	platform.WaitAndClose(ctx)
}
