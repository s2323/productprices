package app

import (
	"context"
	"sync"

	"github.com/go-chi/cors"
	"github.com/go-openapi/spec"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc"

	"bitbucket.org/s2323/productprices/internal/app/productprices"
	"bitbucket.org/s2323/productprices/internal/config"
	"bitbucket.org/s2323/productprices/internal/pkg/database/mongo"
	"bitbucket.org/s2323/productprices/internal/pkg/platform"
	"bitbucket.org/s2323/productprices/internal/pkg/queue"
	"bitbucket.org/s2323/productprices/internal/pkg/worker"
)

// App - приложение (сервис).
type App struct {
	ctx     context.Context
	conf    *config.Config
	impl    *productprices.Implementation
	storage *mongo.DB
	queue   *queue.Queue
	wg      *sync.WaitGroup
}

// New - создаёт новое приложение.
func New(ctx context.Context, conf *config.Config) (a *App, err error) {
	a = &App{
		ctx:  ctx,
		conf: conf,
		wg:   &sync.WaitGroup{},
	}
	platform.AppendCloser(ctx, a)

	a.storage, err = mongo.New(ctx, conf)
	if err != nil {
		return
	}

	workers := make([]queue.Worker, conf.WorkersCount)
	for i := 0; i < conf.WorkersCount; i++ {
		workers[i] = worker.New(a.storage)
	}

	a.queue = queue.New(a.storage, workers...)
	a.impl = productprices.New(a.queue, a.storage)
	return
}

// Close - завершает приложение.
func (a *App) Close() error {
	defer func() {
		logger := platform.GetLoggerFromContext(a.ctx)
		logger.Info("application closed")
	}()

	a.wg.Wait()
	if a.storage != nil {
		return a.storage.Close()
	}
	return nil
}

// RegisterGRPC - регистрирует grpc-сервис.
func (a *App) RegisterGRPC(ctx context.Context, s *grpc.Server) {
	a.impl.RegisterGRPC(ctx, s)
}

// RegisterHTTP - регистрирует http-сервис.
func (a *App) RegisterHTTP(ctx context.Context, mux *runtime.ServeMux) error {
	return a.impl.RegisterHTTP(ctx, mux)
}

// GetSwagger - возвращает описание сервиса.
func (a *App) GetSwagger() (*spec.Swagger, error) {
	return a.impl.GetSwagger()
}

// CORS - возвращает CORS для http-сервиса.
func (a *App) CORS() *cors.Options {
	return &cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{"POST", "OPTIONS"},
		AllowedHeaders: []string{"Accept", "Content-Type"},
	}
}

// Run - запускает выполнение приложения.
func (a *App) Run(ctx context.Context) error {
	a.wg.Add(1)
	go func(ctx context.Context) {
		defer a.wg.Done()
		a.queue.Process(ctx)
	}(ctx)
	return platform.Run(ctx, a)
}
