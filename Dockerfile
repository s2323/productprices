FROM migrate/migrate AS migrate
FROM golang:1.15-alpine3.12 AS builder

WORKDIR /go/src/bitbucket.org/s2323/productprices
COPY . /go/src/bitbucket.org/s2323/productprices/

RUN apk --update upgrade \
    && apk add --no-cache --no-progress git make \
    && rm -rf /var/cache/apk/* \
    && GO111MODULE=on GOPROXY=https://proxy.golang.org go mod download \
    && make build


FROM alpine:3.12

RUN apk --update upgrade \
    && apk add --no-cache --no-progress bash \
    && rm -rf /var/cache/apk/*

COPY --from=migrate /usr/local/bin/migrate /migrate
COPY --from=builder /go/src/bitbucket.org/s2323/productprices/bin/productprices /productprices
COPY --from=builder /go/src/bitbucket.org/s2323/productprices/internal/pkg/database/mongo/migrations/ /migrations/
COPY --from=builder /go/src/bitbucket.org/s2323/productprices/scripts/migrate.sh /migrate.sh

ENTRYPOINT ["/productprices"]
